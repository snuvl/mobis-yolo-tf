Dataset folder
==============

- `caltech-pedestrian-dataset-converter` : https://github.com/mitmul/caltech-pedestrian-dataset-converter
- `CaltechUSA` : dir to Caltech USA dataset

### Instructions

- The directory `caltech-pedestrian-dataset-converter/data` should be linked into `../CaltechUSA`.
- If we run the script, `CaltechUSA/annotations.json` and `CaltechUSA/images` will be created.
