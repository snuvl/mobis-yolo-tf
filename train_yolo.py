"""
Train YOLO !
"""

import os
import tensorflow as tf
import numpy as np
import scipy.io
import time

import caltech_dataset

from yolo import YOLO_TF as YOLO

class YOLO_Train():

    def __init__(self, model):
        self.model = model
        self.session = model.sess
        self.checkpoint_dir = 'checkpoints'
        self.summary_writer = tf.train.SummaryWriter(self.checkpoint_dir, self.session.graph)

    def load_pretrained_weights(self):
        print 'Loading Pretrained weights ...'

    def save_checkpoint(self, current_step):
        print(" [Checkpoint] Saving checkpoints ...")
        model_name = "YOLO"

        checkpoint_dir = os.path.join(self.checkpoint_dir)
        if not os.path.exists(checkpoint_dir):
            os.makedirs(checkpoint_dir)

        checkpoint_path = os.path.join(checkpoint_dir, model_name)
        self.saver.save(self.session, checkpoint_path, global_step=current_step)
        print(" [Checkpoint] Saved checkpoints into %s !" % checkpoint_path)

    def finetune(self):
        model = self.model

        print 'Loading Training dataset ...'
        dataset = caltech_dataset.dataset
        print 'Train set size = %d' % len(dataset.train)
        print 'Test set size = %d' % len(dataset.test)
        batch_size = 10

        print 'Initialize Optimizer and train_op ...'
        _all_variables = tf.all_variables()
        optimizer = tf.train.AdamOptimizer(learning_rate=1e-5)
        self.train_op = optimizer.minimize(model.loss, aggregation_method=2)
        self.session.run(
            tf.initialize_variables([v for v in tf.all_variables() if not v in _all_variables])
        )
        del _all_variables

        print 'Start finetuning ...'
        max_steps = 300000
        self.saver = tf.train.Saver()
        tf.scalar_summary('loss', model.loss)
        merged_summary = tf.merge_all_summaries()

        for step in range(max_steps):
            _t = time.time()
            epoch = step / (len(dataset.train) / float(batch_size))

            batch_ids, batch_images, batch_grid_obj_mask, batch_gt_boxes = dataset.train.next_batch(batch_size)
            feed_dict = {
                model.grid_obj_mask: batch_grid_obj_mask,
                model.gt_boxes: batch_gt_boxes,
                model.x: batch_images,
            }
            [loss, _, summary] = self.session.run([model.loss, self.train_op, merged_summary],
                                                  feed_dict)

            print 'Step %4d (Epoch %.2f) : Loss = %.5f (%.3f sec)' % (step, epoch, loss, time.time() - _t)
            self.summary_writer.add_summary(summary, step)

            # periodic evaluation and checkpoint
            if step % 1000 == 0:
                self.save_checkpoint(step)





def main():
    model = YOLO()
    trainer = YOLO_Train(model)

    #trainer.load_pretrained_weights()
    trainer.finetune()



if __name__ == '__main__':
    main()
