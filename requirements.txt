# vim: set ft=config:
ipython>=4.1.2
jupyter>=1.0.0
easydict>=1.6
tables>=3.2.2
h5py==2.6.0
ipdb>=0.9.3
pudb>=2016.1
Keras>=1.0.1
Lasagne>=0.1
pillow>=3.2.0
matplotlib>=1.5.1
numpy>=1.11.0
pandas>=0.18.0
scikit-image>=0.12.3
scikit-learn>=0.17.1
scipy>=0.17.0
Theano>=0.8.1
colorlog>=2.6.3
hickle>=2.0.5
# tensorflow >= 0.8.0
http://ci.tensorflow.org/view/Nightly/job/nigntly-matrix-linux-gpu/TF_BUILD_CONTAINER_TYPE=GPU,TF_BUILD_IS_OPT=OPT,TF_BUILD_IS_PIP=PIP,TF_BUILD_PYTHON_VERSION=PYTHON2,label=gpu-linux/lastSuccessfulBuild/artifact/pip_test/whl/tensorflow-0.8.0-cp27-none-linux_x86_64.whl
