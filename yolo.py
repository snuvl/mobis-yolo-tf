import numpy as np
import tensorflow as tf
import time
import scipy.misc
import sys


class YOLO_TF:
    fromfile = None
    tofile_img = 'test/output.jpg'
    tofile_txt = 'test/output.txt'
    imshow = True
    filewrite_img = False
    filewrite_txt = False
    disp_console = True
    #weights_file = 'weights/YOLO_small.ckpt'
    weights_file = 'weights/YOLO.ckpt'        # converted version
    alpha = 0.1
    threshold = 0.2
    iou_threshold = 0.5
    num_class = 20
    num_box = 2
    grid_size = 7
    classes =  ["aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat", "chair", "cow",
                "diningtable", "dog", "horse", "motorbike", "person", "pottedplant", "sheep", "sofa", "train","tvmonitor"]

    w_img = 640
    h_img = 480

    def __init__(self, argvs = [], session=None):
        self.argv_parser(argvs)
        self.build_network()
        self.build_loss()
        if session is not None:
            self.sess = session
        else:
            self.sess = self.initialize_session()
        self.load_trained_weights()
        if self.fromfile is not None: self.detect_from_file(self.fromfile)

    def argv_parser(self,argvs):
        for i in range(1,len(argvs),2):
            if argvs[i] == '-fromfile' : self.fromfile = argvs[i+1]
            if argvs[i] == '-tofile_img' : self.tofile_img = argvs[i+1] ; self.filewrite_img = True
            if argvs[i] == '-tofile_txt' : self.tofile_txt = argvs[i+1] ; self.filewrite_txt = True
            if argvs[i] == '-imshow' :
                if argvs[i+1] == '1' :self.imshow = True
                else : self.imshow = False
            if argvs[i] == '-disp_console' :
                if argvs[i+1] == '1' :self.disp_console = True
                else : self.disp_console = False

    def build_network(self):
        print "Building YOLO_small graph ..."
        self.vars = {}

        with tf.variable_scope("YOLO"):
            self.x = tf.placeholder('float32',  [None, 448, 448, 3],
                                    name='image_input')
            self.images = self.x

            self.conv_1 = self.conv_layer(1, self.x, 64, 7, 2)
            self.pool_2 = self.pool_layer(2, self.conv_1, 2, 2)
            self.conv_3 = self.conv_layer(3, self.pool_2, 192, 3, 1)
            self.pool_4 = self.pool_layer(4, self.conv_3, 2, 2)
            self.conv_5 = self.conv_layer(5, self.pool_4, 128, 1, 1)
            self.conv_6 = self.conv_layer(6, self.conv_5, 256, 3, 1)
            self.conv_7 = self.conv_layer(7, self.conv_6, 256, 1, 1)
            self.conv_8 = self.conv_layer(8, self.conv_7, 512, 3, 1)
            self.pool_9 = self.pool_layer(9, self.conv_8, 2, 2)
            self.conv_10 = self.conv_layer(10, self.pool_9, 256, 1, 1)
            self.conv_11 = self.conv_layer(11, self.conv_10, 512, 3, 1)
            self.conv_12 = self.conv_layer(12, self.conv_11, 256, 1, 1)
            self.conv_13 = self.conv_layer(13, self.conv_12, 512, 3, 1)
            self.conv_14 = self.conv_layer(14, self.conv_13, 256, 1, 1)
            self.conv_15 = self.conv_layer(15, self.conv_14, 512, 3, 1)
            self.conv_16 = self.conv_layer(16, self.conv_15, 256, 1, 1)
            self.conv_17 = self.conv_layer(17, self.conv_16, 512, 3, 1)
            self.conv_18 = self.conv_layer(18, self.conv_17, 512, 1, 1)
            self.conv_19 = self.conv_layer(19, self.conv_18, 1024, 3, 1)
            self.pool_20 = self.pool_layer(20, self.conv_19, 2, 2)
            self.conv_21 = self.conv_layer(21, self.pool_20, 512, 1, 1)
            self.conv_22 = self.conv_layer(22, self.conv_21, 1024, 3, 1)
            self.conv_23 = self.conv_layer(23, self.conv_22, 512, 1, 1)
            self.conv_24 = self.conv_layer(24, self.conv_23, 1024, 3, 1)
            self.conv_25 = self.conv_layer(25, self.conv_24, 1024, 3, 1)
            self.conv_26 = self.conv_layer(26, self.conv_25, 1024, 3, 2)
            self.conv_27 = self.conv_layer(27, self.conv_26, 1024, 3, 1)
            self.conv_28 = self.conv_layer(28, self.conv_27, 1024, 3, 1)
            self.fc_29 = self.fc_layer(29, self.conv_28, 512, flat=True, linear=False)
            self.fc_30 = self.fc_layer(30, self.fc_29, 4096, flat=False, linear=False)
            #skip dropout_31
            self.fc_32 = self.fc_layer(32, self.fc_30, 1470, flat=False, linear=True)


    def build_loss(self):
        S, B = self.grid_size, self.num_box

        batch_output = self.fc_32  # [-1, 1470]
        pred_boxes = tf.reshape(batch_output[:, 980+98:], [-1, 7, 7, 2, 4],
                                name='pred_boxes')
        pred_confidence = tf.reshape(batch_output[:, 980:980+98], [-1, 7, 7, 2],
                                     name='pred_confidence')

        # (0) prepare inputs
        self.grid_obj_mask = tf.placeholder(tf.uint8, [None, 7, 7, 2],
                                            name='grid_obj_mask')
        grid_obj_mask = tf.cast(self.grid_obj_mask, tf.float32)

        #self.grid_mask = tf.placeholder(tf.uint8, [None, 7, 7])
        #grid_noobj_mask = 1.0 - grid_obj_mask
        self.gt_boxes = tf.placeholder(tf.float32, [None, 7, 7, 2, 4],
                                       name='gt_boxes')

        # (1) regression on [x, y, sqrt{w}, sqrt{h}]
        loss_box = tf.reduce_mean(
            tf.mul(tf.expand_dims(grid_obj_mask, [4]), # [-1, 7, 7, 2, '1']
                   (pred_boxes - self.gt_boxes) ** 2.0),
            name='loss_box')

        # (2) regression on confidence
        loss_confidence_obj = tf.reduce_mean(
            tf.mul(grid_obj_mask, (pred_confidence - 1.0) ** 2.0),
            name='loss_confidence_obj')

        loss_confidence_noobj = tf.reduce_mean(
            tf.mul(1 - grid_obj_mask, (pred_confidence - 0.0) ** 2.0),
            name='loss_confidence_noobj')

        ## (3) regression on class probability
        #loss_class = tf.reduce_mean(
        #    tf.mul(grid_obj_mask, (pred_class_prob - self.gt_class_prob) ** 2.0), # can broadcast [7,7] -> [7,7,:]?
        #    name='loss_class')

        self.loss = tf.add_n([
            5.0 * loss_box,
            1.0 * loss_confidence_obj,
            0.5 * loss_confidence_noobj,
            #1.0 * loss_class,
        ], name='loss')

        return self.loss


    def initialize_session(self):
        sess = tf.Session(config=tf.ConfigProto(
            gpu_options=tf.GPUOptions(allow_growth=True),
            device_count={'GPU': 1},
        ))
        sess.run(tf.initialize_all_variables())
        return sess

    def load_trained_weights(self):
        print "Loading pretrained model from %s ..." % self.weights_file
        self.saver = tf.train.Saver()
        self.saver.restore(self.sess,self.weights_file)
        print "Loading complete!" + '\n'

    def conv_layer(self, idx, inputs, filters, size, stride):
        with tf.variable_scope('conv_%d' % idx):
            channels = int(inputs.get_shape()[3])

            weight = tf.get_variable('weight', [size, size, channels, filters],
                                    initializer=tf.truncated_normal_initializer(0, 0.1))
            biases = tf.get_variable('bias', [filters],
                                    initializer=tf.constant_initializer(0.1))
            self.vars[weight.name] = weight
            self.vars[biases.name] = biases

            pad_size = size // 2
            pad_mat = np.array([[0, 0], [pad_size, pad_size], [pad_size, pad_size], [0, 0]])
            inputs_pad = tf.pad(inputs, pad_mat)

            conv = tf.nn.conv2d(inputs_pad, weight, strides=[1, stride, stride, 1], padding='VALID') + biases
            print '    Layer  %d : Type = Conv, Size = %d * %d, Stride = %d, Filters = %d, Input channels = %d' \
                % (idx,size,size,stride,filters,channels)
            #print weight.get_shape(), biases.get_shape()

            return tf.maximum(self.alpha * conv, conv, name='leaky_relu')

    def pool_layer(self, idx, inputs, size, stride):
        with tf.variable_scope('pool_%d' % idx):
            print '    Layer  %d : Type = Pool, Size = %d * %d, Stride = %d' % (idx, size, size, stride)
            return tf.nn.max_pool(inputs, ksize=[1, size, size, 1], strides=[1, stride, stride, 1],
                                  padding='SAME', name='pool')

    def fc_layer(self, idx, inputs, hiddens, flat=False, linear=False):
        input_shape = inputs.get_shape().as_list()
        if flat:
            dim = input_shape[1] * input_shape[2] * input_shape[3]
            inputs_transposed = tf.transpose(inputs,(0,3,1,2))
            inputs_processed = tf.reshape(inputs_transposed, [-1, dim])
        else:
            dim = input_shape[1]
            inputs_processed = inputs

        with tf.variable_scope('fc_%d' % idx):
            weight = tf.get_variable('weight', [dim, hiddens],
                                    initializer=tf.truncated_normal_initializer(0, 0.1))
            biases = tf.get_variable('bias', [hiddens],
                                    initializer=tf.constant_initializer(0.1))
            self.vars[weight.name] = weight
            self.vars[biases.name] = biases

            print '    Layer  %d : Type = Full, Hidden = %d, Input dimension = %d, Flat = %d, Activation = %d' % (idx,hiddens,int(dim),int(flat),1-int(linear))
            #print weight.get_shape(), biases.get_shape()

            if linear:
                return tf.add(tf.matmul(inputs_processed, weight), biases, name='fc')

            ip = tf.add(tf.matmul(inputs_processed, weight), biases)
            return tf.maximum(self.alpha * ip, ip, name='fc_leaky_relu')

    def detect_image(self,img):
        assert img.dtype == np.float32 and len(img.shape) == 3

        s = time.time()
        self.h_img, self.w_img,_ = img.shape
        img_resized = scipy.misc.imresize(img, (448, 448))
        #img_RGB = cv2.cvtColor(img_resized,cv2.COLOR_BGR2RGB)
        img_RGB = img_resized
        img_resized_np = np.asarray( img_RGB )
        inputs = np.zeros((1,448,448,3),dtype='float32')
        inputs[0] = (img_resized_np/255.0)*2.0-1.0     # (-1, 1) scale
        in_dict = {self.x: inputs}
        net_output = self.sess.run(self.fc_32,feed_dict=in_dict)
        self.result = self.interpret_output(net_output[0])
        self.show_results(img,self.result)
        strtime = str(time.time()-s)
        print 'Elapsed time : ' + strtime + ' secs' + '\n'

    def detect_from_file(self,filename):
        print 'Detect from ' + filename
        img = scipy.misc.imread(filename)
        img = img.astype(np.float32) / 255.0
        self.detect_image(img)

    def interpret_output(self,output):
        assert output.shape == (1470, )

        S = self.grid_size # 7
        B = self.num_box
        K = self.num_class # 20
        assert 980 == S * S * K

        probs = np.zeros((S, S, B, K))  # (7, 7, 2, 20)
        class_probs = np.reshape(output[0:980], (7,7,20))

        scales = np.reshape(output[980:980+98], (7,7,2))
        boxes = np.reshape(output[980+98:], (7,7,2,4))
        offset = np.transpose(np.reshape(np.array([np.arange(7)]*14),(2,7,7)),(1,2,0))

        boxes[:,:,:,0] += offset
        boxes[:,:,:,1] += np.transpose(offset,(1,0,2))
        boxes[:,:,:,0:2] = boxes[:,:,:,0:2] / float(S)
        boxes[:,:,:,2] = np.multiply(boxes[:,:,:,2],boxes[:,:,:,2])
        boxes[:,:,:,3] = np.multiply(boxes[:,:,:,3],boxes[:,:,:,3])

        boxes[:,:,:,0] *= self.w_img
        boxes[:,:,:,1] *= self.h_img
        boxes[:,:,:,2] *= self.w_img
        boxes[:,:,:,3] *= self.h_img

        for i in range(self.num_box): # 2
            for j in range(self.num_class): # 20
                probs[:,:,i,j] = np.multiply(class_probs[:,:,j],scales[:,:,i])

        filter_mat_probs = np.array(probs>=self.threshold,dtype='bool')
        filter_mat_boxes = np.nonzero(filter_mat_probs)
        boxes_filtered = boxes[filter_mat_boxes[0],filter_mat_boxes[1],filter_mat_boxes[2]]
        probs_filtered = probs[filter_mat_probs]
        classes_num_filtered = np.argmax(filter_mat_probs,axis=3)[filter_mat_boxes[0],filter_mat_boxes[1],filter_mat_boxes[2]]

        argsort = np.array(np.argsort(probs_filtered))[::-1]
        boxes_filtered = boxes_filtered[argsort]
        probs_filtered = probs_filtered[argsort]
        classes_num_filtered = classes_num_filtered[argsort]

        for i in range(len(boxes_filtered)):
            if probs_filtered[i] == 0 : continue
            for j in range(i+1,len(boxes_filtered)):
                if self.iou(boxes_filtered[i],boxes_filtered[j]) > self.iou_threshold :
                    probs_filtered[j] = 0.0

        filter_iou = np.array(probs_filtered>0.0,dtype='bool')
        boxes_filtered = boxes_filtered[filter_iou]
        probs_filtered = probs_filtered[filter_iou]
        classes_num_filtered = classes_num_filtered[filter_iou]

        result = []
        for i in range(len(boxes_filtered)):
            result.append([self.classes[classes_num_filtered[i]],boxes_filtered[i][0],boxes_filtered[i][1],boxes_filtered[i][2],boxes_filtered[i][3],probs_filtered[i]])

        return result

    def show_results(self,img,results):
        import matplotlib.pyplot as plt
        if self.filewrite_txt :
            ftxt = open(self.tofile_txt,'w')

        _, ax = plt.subplots(1)
        ax.imshow(img)

        for i in range(len(results)):
            x = int(results[i][1])
            y = int(results[i][2])
            w = int(results[i][3])
            h = int(results[i][4])
            print('    class : ' + results[i][0] + ' , [x,y,w,h]=[' + \
                    str(x) + ',' + str(y) + ',' + str(int(results[i][3])) + ',' + \
                    str(int(results[i][4]))+'], Confidence = ' + str(results[i][5])
                  )

            ax.add_patch(
                plt.Rectangle((x-w/2, y-h/2), w, h, fill=False, edgecolor='r', linewidth=3)
            )
            label = results[i][0] + ' : %.2f' % results[i][5]
            ax.text(x-w/2, y-h/2, label, fontsize=12, bbox={'facecolor': 'r', 'alpha': 0.3}, color='white')

        if self.filewrite_txt :
            ftxt.write(results[i][0] + ',' + str(x) + ',' + str(y) + ',' + str(w) + ',' + str(h)+',' + str(results[i][5]) + '\n')

        plt.show()

        if self.filewrite_txt:
            print '    txt file writed : ' + self.tofile_txt
            ftxt.close()

    def iou(self,box1,box2):
        tb = min(box1[0]+0.5*box1[2],box2[0]+0.5*box2[2])-max(box1[0]-0.5*box1[2],box2[0]-0.5*box2[2])
        lr = min(box1[1]+0.5*box1[3],box2[1]+0.5*box2[3])-max(box1[1]-0.5*box1[3],box2[1]-0.5*box2[3])
        if tb < 0 or lr < 0 : intersection = 0
        else : intersection =  tb*lr
        return intersection / (box1[2]*box1[3] + box2[2]*box2[3] - intersection)

    def training(self): #TODO add training function!
        return None




def main(argvs):
    yolo = YOLO_TF(argvs)


if __name__=='__main__':
    main(sys.argv)
