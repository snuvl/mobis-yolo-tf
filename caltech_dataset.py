import numpy as np
import pandas as pd
import random
import scipy.misc
import os.path
import json
from bisect import bisect
from glob import glob

__PATH__ = os.path.abspath(os.path.dirname(__file__))
join = os.path.join

class CaltechDetectionDataset:

    def __init__(self, basepath, train=True):
        self.basepath = basepath

        images = glob(join(basepath, 'images', '*.png'))
        assert len(images) > 0, ('Not exists : ' + basepath)

        def _is_in_train_split(f):
            f = os.path.basename(f)
            assert f[:3] == 'set'
            set_no = int(f[3:5])
            return 0 <= set_no <= 5

        if train:
            images = [f for f in images if _is_in_train_split(f)]
        else:
            images = [f for f in images if not _is_in_train_split(f)]

        # store the list of image ids (e.g. ['001100', '001101', ...]
        self.ids = []
        for f in sorted(images):
            id = os.path.splitext(os.path.basename(f))[0]
            self.ids.append(id)

        # read annotation JSON
        with open(join(basepath, 'annotations.json')) as fp:
            self.anno = json.load(fp)

    def __repr__(self):
        s = 'CaltechDetectionDataset with %d images' % len(self)
        return s

    def __len__(self):
        return len(self.ids)

    def image_path(self, id):
        '''Return the path to image (png) given id.'''
        return join(self.basepath, 'images', '%s.png' % id)

    def load_image(self, id):
        '''Return the image as np.ndarray given id.'''
        im = scipy.misc.imread(self.image_path(id)).astype(np.float32)
        im /= 255.0
        return im

    def visualize(self, id, show_bounding_box=True):
        import matplotlib.pyplot as plt
        im = self.load_image(id)

        _, ax = plt.subplots(1)
        ax.imshow(im)

        #df = self.get_label_df(id)
        anno_objects = self.get_annotation(id)
        for obj in anno_objects:
            color = 'y'
            name = obj['lbl']
            if name == 'person': color = 'r'
            if name == 'people': color = 'r'

            if show_bounding_box:
                xmin, ymin, w, h = obj['pos']
                ax.add_patch(
                    plt.Rectangle((xmin, ymin), w, h,
                                  fill=False, edgecolor=color, linewidth=3)
                )
                label = '%s (%s)' % (obj['lbl'], obj['str'])
                ax.text(xmin, ymin, label, fontsize=12,
                        bbox={'facecolor':color, 'alpha':0.3},
                        color='white')
        plt.show()

    def get_annotation(self, id):
        '''
        Get object annotation for the given image id
        '''
        # id is in form 'set00_V007_1323'
        set_id, vid_id, frm_id = id.split('_')
        assert set_id[:3] == 'set' and vid_id[0] == 'V'

        _frames = self.anno[set_id][vid_id]['frames']
        if frm_id in _frames:
            anno_objects = _frames[frm_id]
        else:
            anno_objects = []

        return anno_objects

    def batch_input_iterator(self, grid_size=(7, 7), box_per_grid=2, image_size=(448, 448)):
        '''
        Prepares all the ndarray inputs that can fed into YOLO's training code.
        TODO: Model-specific implementation (strongly coupled to YOLO's implementation)

        (1) grid_obj_mask [7, 7, 2]
        (2) gt_boxes [7, 7, 2, 4] : x, y, sqrt{w}, sqrt{h}
        '''
        ids = list(self.ids)
        random.shuffle(ids)

        GH, GW = grid_size
        B = box_per_grid

        for id in ids:
            # image (later resized in 448x448)
            im = self.load_image(id)
            IH, IW = im.shape[:2]      # size, the original scale

            im = scipy.misc.imresize(im, image_size)
            im = im.astype(np.float32) / 255.0
            assert im.dtype == np.float32
            im = im * 2 - 1.0 # XXX [-1, 1] scale

            # objects
            grid_linspace_h = np.linspace(0, IH, GH+1)
            grid_linspace_w = np.linspace(0, IW, GW+1)
            gt_boxes = np.zeros((GH, GW, B, 4), dtype=np.float32)
            grid_obj_mask = np.zeros((GH, GW, B), dtype=np.uint8)
            obj_cnt = np.zeros((GH, GW), dtype=np.uint8)

            for obj in self.get_annotation(id):
                xmin, ymin, w, h = obj['pos']
                x_center, y_center = xmin + w / 2.0, ymin + h / 2.0
                w_sqrt, h_sqrt = np.sqrt(float(w) / IW), np.sqrt(float(h) / IH)

                # determine where (x_center, y_center) belongs to which grid cell (i, j)
                grid_i = bisect(grid_linspace_h, y_center) - 1
                grid_j = bisect(grid_linspace_w, x_center) - 1

                #raise ValueError('grid_i = %d, grid_j = %d' % (grid_i, grid_j))
                # some data may has invalid x_center, y_center data. ignore them
                if not (0 <= grid_i < GH): grid_i = max(0, min(grid_i, GH - 1))
                if not (0 <= grid_j < GW): grid_j = max(0, min(grid_j, GW - 1))

                # convert to relative coordinate.
                x_ = (float(x_center) / IW * GW) - grid_j
                y_ = (float(y_center) / IH * GH) - grid_i
                #assert 0 <= x_ < 1.0

                if obj_cnt[grid_i, grid_j] < B:
                    b = obj_cnt[grid_i, grid_j]
                    grid_obj_mask[grid_i, grid_j, b] = 1
                    gt_boxes[grid_i, grid_j, b, :] = [x_, y_, w_sqrt, h_sqrt]

                    obj_cnt[grid_i, grid_j] += 1

            yield id, im, grid_obj_mask, gt_boxes



    def next_batch(self, batch_size, grid_size=(7, 7), box_per_grid=2, image_size=(448, 448)):
        GH, GW = grid_size
        B = box_per_grid

        batch_ids = np.zeros((batch_size,), dtype=object)
        batch_images = np.zeros((batch_size, image_size[0], image_size[1], 3),
                                dtype=np.float32)
        batch_grid_obj_mask = np.zeros((batch_size, GH, GW, B),
                                       dtype=np.uint8)
        batch_gt_boxes = np.zeros((batch_size, GH, GW, B, 4),
                                  dtype=np.float32)

        if not hasattr(self, 'batch_it'):
            self.batch_it = self.batch_input_iterator()

        for k in range(batch_size):
            try:
                _it_ret = next(self.batch_it)
            except StopIteration:
                self.batch_it = self.batch_input_iterator()
                _it_ret = next(self.batch_it)

            (batch_ids[k],
             batch_images[k, :, :],
             batch_grid_obj_mask[k, :, :, :],
             batch_gt_boxes[k, :, :, :, :]) = _it_ret

        return batch_ids, batch_images, batch_grid_obj_mask, batch_gt_boxes



class CaltechDetectionDatasets:

    def __init__(self):
        self._train = None
        self._test = None

    @property
    def train(self):
        if self._train is None:
            self._train = CaltechDetectionDataset(
                join(__PATH__, 'dataset/CaltechUSA'),
                train=True
            )
        return self._train

    @property
    def test(self):
        if self._test is None:
            self._test = CaltechDetectionDataset(
                join(__PATH__, 'dataset/CaltechUSA'),
                train=False
            )
        return self._train

dataset = CaltechDetectionDatasets()
