'''
Dirty Script for converting legacy YOLO model to our name.

Read checkpoint YOLO_small.ckpt and produces YOLO_small.ckpt.new
'''
import numpy as np
import tensorflow as tf
import time
import scipy.misc
import sys


class YOLO_TF:
    fromfile = None
    tofile_img = 'test/output.jpg'
    tofile_txt = 'test/output.txt'
    imshow = True
    filewrite_img = False
    filewrite_txt = False
    disp_console = True
    weights_file = 'weights/YOLO_small.ckpt'
    alpha = 0.1
    threshold = 0.2
    iou_threshold = 0.5
    num_class = 20
    num_box = 2
    grid_size = 7
    classes =  ["aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat", "chair", "cow",
                "diningtable", "dog", "horse", "motorbike", "person", "pottedplant", "sheep", "sofa", "train","tvmonitor"]

    w_img = 640
    h_img = 480

    def __init__(self,argvs = []):
        self.build_network()
        self.initialize_session()

    def build_network(self):
        print "Building YOLO_small graph ..."

        self.ckpt = tf.train.NewCheckpointReader('./weights/YOLO_small.ckpt')
        self.legacy_mapping = dict()
        self.vid = 0

        with tf.variable_scope("YOLO"):
            self.x = tf.placeholder('float32',  [None, 448, 448, 3])
            self.conv_1 = self.conv_layer(1, self.x, 64, 7, 2)
            self.pool_2 = self.pool_layer(2, self.conv_1, 2, 2)
            self.conv_3 = self.conv_layer(3, self.pool_2, 192, 3, 1)
            self.pool_4 = self.pool_layer(4, self.conv_3, 2, 2)
            self.conv_5 = self.conv_layer(5, self.pool_4, 128, 1, 1)
            self.conv_6 = self.conv_layer(6, self.conv_5, 256, 3, 1)
            self.conv_7 = self.conv_layer(7, self.conv_6, 256, 1, 1)
            self.conv_8 = self.conv_layer(8, self.conv_7, 512, 3, 1)
            self.pool_9 = self.pool_layer(9, self.conv_8, 2, 2)
            self.conv_10 = self.conv_layer(10, self.pool_9, 256, 1, 1)
            self.conv_11 = self.conv_layer(11, self.conv_10, 512, 3, 1)
            self.conv_12 = self.conv_layer(12, self.conv_11, 256, 1, 1)
            self.conv_13 = self.conv_layer(13, self.conv_12, 512, 3, 1)
            self.conv_14 = self.conv_layer(14, self.conv_13, 256, 1, 1)
            self.conv_15 = self.conv_layer(15, self.conv_14, 512, 3, 1)
            self.conv_16 = self.conv_layer(16, self.conv_15, 256, 1, 1)
            self.conv_17 = self.conv_layer(17, self.conv_16, 512, 3, 1)
            self.conv_18 = self.conv_layer(18, self.conv_17, 512, 1, 1)
            self.conv_19 = self.conv_layer(19, self.conv_18, 1024, 3, 1)
            self.pool_20 = self.pool_layer(20, self.conv_19, 2, 2)
            self.conv_21 = self.conv_layer(21, self.pool_20, 512, 1, 1)
            self.conv_22 = self.conv_layer(22, self.conv_21, 1024, 3, 1)
            self.conv_23 = self.conv_layer(23, self.conv_22, 512, 1, 1)
            self.conv_24 = self.conv_layer(24, self.conv_23, 1024, 3, 1)
            self.conv_25 = self.conv_layer(25, self.conv_24, 1024, 3, 1)
            self.conv_26 = self.conv_layer(26, self.conv_25, 1024, 3, 2)
            self.conv_27 = self.conv_layer(27, self.conv_26, 1024, 3, 1)
            self.conv_28 = self.conv_layer(28, self.conv_27, 1024, 3, 1)
            self.fc_29 = self.fc_layer(29, self.conv_28, 512, flat=True, linear=False)
            self.fc_30 = self.fc_layer(30, self.fc_29, 4096, flat=False, linear=False)
            #skip dropout_31
            self.fc_32 = self.fc_layer(32, self.fc_30, 1470, flat=False, linear=True)

    def initialize_session(self):
        self.sess = tf.Session(config=tf.ConfigProto(
            gpu_options=tf.GPUOptions(allow_growth=True),
            device_count={'GPU': 1},
        ))
        self.sess.run(tf.initialize_all_variables())
        self.saver = tf.train.Saver()
        #self.saver.restore(self.sess,self.weights_file)
        for v in tf.trainable_variables():
            value = self.ckpt.get_tensor(self.legacy_mapping[v])
            print v.name, self.legacy_mapping[v], value.shape
            self.sess.run(v.assign(value))

        # XXX save the converted weights
        self.saver.save(self.sess, self.weights_file + '.new', write_meta_graph=False)
        if self.disp_console : print "Loading complete!" + '\n'

    def get_legacy_variable(self):
        if self.vid == 0:
            s = 'Variable'
        else:
            s = 'Variable_%d' % self.vid
        self.vid += 1
        return s

    def conv_layer(self, idx, inputs, filters, size, stride):
        with tf.variable_scope('conv_%d' % idx):
            channels = int(inputs.get_shape()[3])

            weight = tf.get_variable('weight', [size, size, channels, filters],
                                    initializer=tf.truncated_normal_initializer(0, 0.1))
            biases = tf.get_variable('bias', [filters],
                                    initializer=tf.constant_initializer(0.1))

            # XXX
            self.legacy_mapping[weight] = self.get_legacy_variable()
            self.legacy_mapping[biases] = self.get_legacy_variable()
            # XXX

            pad_size = size // 2
            pad_mat = np.array([[0, 0], [pad_size, pad_size], [pad_size, pad_size], [0, 0]])
            inputs_pad = tf.pad(inputs, pad_mat)

            conv = tf.nn.conv2d(inputs_pad, weight, strides=[1, stride, stride, 1], padding='VALID') + biases
            print '    Layer  %d : Type = Conv, Size = %d * %d, Stride = %d, Filters = %d, Input channels = %d' \
                % (idx,size,size,stride,filters,channels)

            return tf.maximum(self.alpha * conv, conv, name='leaky_relu')

    def pool_layer(self, idx, inputs, size, stride):
        with tf.variable_scope('pool_%d' % idx):
            print '    Layer  %d : Type = Pool, Size = %d * %d, Stride = %d' % (idx, size, size, stride)
            return tf.nn.max_pool(inputs, ksize=[1, size, size, 1], strides=[1, stride, stride, 1],
                                  padding='SAME', name='pool')

    def fc_layer(self, idx, inputs, hiddens, flat=False, linear=False):
        input_shape = inputs.get_shape().as_list()
        if flat:
            dim = input_shape[1] * input_shape[2] * input_shape[3]
            inputs_transposed = tf.transpose(inputs,(0,3,1,2))
            inputs_processed = tf.reshape(inputs_transposed, [-1, dim])
        else:
            dim = input_shape[1]
            inputs_processed = inputs

        with tf.variable_scope('fc_%d' % idx):
            weight = tf.get_variable('weight', [dim, hiddens],
                                    initializer=tf.truncated_normal_initializer(0, 0.1))
            biases = tf.get_variable('bias', [hiddens],
                                    initializer=tf.constant_initializer(0.1))

            # XXX
            self.legacy_mapping[weight] = self.get_legacy_variable()
            self.legacy_mapping[biases] = self.get_legacy_variable()
            # XXX

            print '    Layer  %d : Type = Full, Hidden = %d, Input dimension = %d, Flat = %d, Activation = %d' % (idx,hiddens,int(dim),int(flat),1-int(linear))

            if linear:
                return tf.add(tf.matmul(inputs_processed, weight), biases, name='fc')

            ip = tf.add(tf.matmul(inputs_processed, weight), biases)
            return tf.maximum(self.alpha * ip, ip, name='fc_leaky_relu')




def main(argvs):
    yolo = YOLO_TF(argvs)


if __name__=='__main__':
    main(sys.argv)
