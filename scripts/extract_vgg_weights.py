import numpy as np
import scipy.io
import tensorflow as tf

vgg_graph = tf.Graph()

with vgg_graph.as_default():
    session = tf.Session(config=tf.ConfigProto(
        gpu_options=tf.GPUOptions(allow_growth=True),
        device_count={'GPU': 1},
    ))

    vgg_graph_def = tf.GraphDef()
    with open('./vgg16.tfmodel', "rb") as fp:
        vgg_graph_def.ParseFromString(fp.read())
        tf.import_graph_def(vgg_graph_def, name="")

    target_var_names = []
    for op in vgg_graph.get_operations():
        opname = op.name
        if opname.endswith('/filter') or opname.endswith('/weight') or opname.endswith('/bias'):
            target_var_names.append(op.name)

    weights = {}
    for var_name in target_var_names:
        op = vgg_graph.get_operation_by_name(var_name)
        constant_tensor = op.values()[0]
        print constant_tensor.name, constant_tensor.get_shape()
        constant_np = session.run(constant_tensor)
        weights[var_name] = constant_np

    scipy.io.savemat('./vgg16.weights.mat', weights)
    print 'Done!'
