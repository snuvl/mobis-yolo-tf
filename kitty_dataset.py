import numpy as np
import pandas as pd
import scipy.misc
import os.path
from glob import glob

__PATH__ = os.path.abspath(os.path.dirname(__file__))
join = os.path.join

class KittyDetectionDataset:

    def __init__(self, image_basepath, label_basepath, target_classes=None):
        self.image_basepath = image_basepath
        self.label_basepath = label_basepath

        images = sorted(glob(join(image_basepath, '*.png')))
        if label_basepath:
            labels = sorted(glob(join(label_basepath, '*.txt')))
        else: # no annotation is given (i.e. test set)
            labels = None

        if labels:
            assert len(images) == len(labels), \
                'len(images) = {}, len(labels) = {}'.format(len(images), len(labels))

        # store the list of image ids (e.g. ['001100', '001101', ...]
        self.ids = []
        for f in images:
            id = os.path.splitext(os.path.basename(f))[0]
            self.ids.append(id)

        if labels:
            self.df_labels = self.load_labels()
        else:
            self.df_labels = None

    def load_labels(self):
        '''
        Read and all the labels in a pandas DataFrame format.
        '''
        assert self.label_path is not None

        def _iter_df():
            for k, id in enumerate(self.ids):
                df = pd.read_csv(self.label_path(id), sep=' ', header=None,
                                names=['name', 'truncated', 'occuled', 'alpha',
                                        'xmin', 'ymin', 'xmax', 'ymax',
                                        'dim_height', 'dim_weight', 'dim_length',
                                        'loc_x', 'loc_y', 'loc_z', 'rotation_y'],
                                )
                df.insert(0, 'image_id', id)
                df.insert(1, 'obj_index', range(len(df)))
                yield df

        df_all = pd.concat(_iter_df(), ignore_index=True)
        return df_all

    def __repr__(self):
        s = 'KittyDetectionDataset with %d images' % len(self)
        return s

    def __len__(self):
        return len(self.ids)

    def image_path(self, id):
        '''Return the path to image (png) given id.'''
        return join(self.image_basepath, '%s.png' % id)

    def label_path(self, id):
        '''Return the path to annotation file (txt) given id.'''
        return join(self.label_basepath, '%s.txt' % id)

    def load_image(self, id):
        '''Return the image as np.ndarray given id.'''
        im = scipy.misc.imread(self.image_path(id)).astype(np.float32)
        im /= 255.0
        return im

    def visualize(self, id, show_bounding_box=True):
        import matplotlib.pyplot as plt
        im = self.load_image(id)

        _, ax = plt.subplots(1)
        ax.imshow(im)

        df = self.get_label_df(id)
        for k, v in df.T.iteritems():
            name = v['name'].lower()

            color = 'y'
            if name == 'pedestrian': color = 'r'
            if name == 'cyclist': color = 'g'
            if name == 'car': color = 'b'

            if show_bounding_box:
                ax.add_patch(
                    plt.Rectangle((v.xmin, v.ymin), v.xmax - v.xmin, v.ymax - v.ymin,
                                fill=False, edgecolor=color, linewidth=3)
                )
                label = '%s (%s)' % (v['name'], v['obj_index'])
                ax.text(v.xmin, v.ymin, label, fontsize=12,
                        bbox={'facecolor':color, 'alpha':0.3},
                        color='white')
        plt.show()

    def get_label_df(self, id):
        '''
        Get the pandas DataFrame object for the specified instance.
        '''
        df = self.df_labels[self.df_labels.image_id == id]
        if len(df) == 0:
            raise ValueError('No annotation exists for %s' % id)
        return df


def get_dataset(train_or_test):
    if train_or_test == 'train':
        train = KittyDetectionDataset(
            join(__PATH__, 'dataset/kitty/training/image_2'),
            join(__PATH__, 'dataset/kitty/training/label_2'),
            target_classes = ('Pedestrian'),
        )
        return train
    elif train_or_test == 'test':
        test = KittyDetectionDataset(
            join(__PATH__, 'dataset/kitty/testing/image_2'),
            None, # no labels
            target_classes = ('Pedestrian'),
        )
        return test
    else:
        raise ValueError(train_or_test)
